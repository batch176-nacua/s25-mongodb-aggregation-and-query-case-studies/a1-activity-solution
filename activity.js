//Activity (Aggregattion)
//Total # of items supplied by Red Farms
db.fruits.aggregate([
    {$match: {supplier: "Red Farms Inc."} },
    {$count: "# of fruits supplied from Red Farms Inc"}
])
//Total # of items where price > 50
db.fruits.aggregate([
    {$match: {price: {$gt: 50} } },
    {$count: "# of items where price is less than 50"}
])
//Average Price of all fruits that are onSale
db.fruits.aggregate([
    {$match: {onSale: true} },
    {$group: {_id:"$supplier", avgPrice: {$avg: "$price"} } }
])
//Highest price of fruits that are on sale per supplier
db.fruits.aggregate([
    {$match: {onSale: true} },
    {$group: {_id: "$supplier", maxPrice: {$max: "$price"} } }
])
//Lowest price of fruits that are on sale per supplier
db.fruits.aggregate([
    {$match: {onSale: true} },
    {$group: {_id: "$supplier", minPrice: {$min: "$price"} } }
]) 